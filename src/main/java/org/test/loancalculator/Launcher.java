package org.test.loancalculator;

import org.test.loancalculator.calculators.ImmediateAnnuityPaymentCalculator;
import org.test.loancalculator.calculators.LoanCalculator;
import org.test.loancalculator.calculators.LoanParamsVerifier;
import org.test.loancalculator.calculators.VolumeWeightedRateCalculator;
import org.test.loancalculator.objects.CalculationResult;
import org.test.loancalculator.objects.Lender;
import org.test.loancalculator.objects.LoanInfo;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

public class Launcher {

	private static final int FIXED_PERIOD = 36;

	public static void main(String... args) {

		if (args.length < 2) {
			printError("Please use command format: quote [market_file] [loan_amount]");
			return;
		}

		Collection<Lender> lenders;
		try {
			lenders = LenderPoolLoader.loadFromFile(new File(args[0]));
		} catch (IOException e) {
			printError("Can't read file: " + e.getMessage());
			return;
		}

		BigDecimal loanAmount;
		try {
			int parsedParam = Integer.parseInt(args[1]);
			loanAmount = BigDecimal.valueOf(parsedParam).setScale(2, RoundingMode.HALF_UP);
		} catch (NumberFormatException e) {
			printError("Wrong loan amount format. Please use integer");
			return;
		}

		LoanCalculator calculator = new LoanCalculator(
				new VolumeWeightedRateCalculator(lenders),
				new ImmediateAnnuityPaymentCalculator(),
				createVerifiers(lenders));

		CalculationResult calculationResult = calculator.calculateLoan(loanAmount, FIXED_PERIOD);
		if (calculationResult.isSuccess()) {
			printLoanInfo(calculationResult.getResult());
		} else {
			printError(calculationResult.getErrorDescription());
		}
	}

	private static List<LoanParamsVerifier> createVerifiers(Collection<Lender> lenders) {

		LoanParamsVerifier edgesVerifier = (amount, period) ->
				amount.compareTo(BigDecimal.valueOf(1000)) < 0 || amount.compareTo(BigDecimal.valueOf(15000)) > 0
						? "Please use loan amount from \u00A31000 to \u00A315000"
						: null;

		LoanParamsVerifier incrementVerifier = (amount, period) ->
				amount.remainder(BigDecimal.valueOf(100)).compareTo(BigDecimal.ZERO) != 0
						? "Loan amount must be a multiple of \u00A3100"
						: null;

		LoanParamsVerifier amountAvailableVerifier = (amount, period) -> {
			BigDecimal availableAmount = lenders.stream()
					.map(Lender::getAvailableAmount)
					.reduce(BigDecimal.ZERO, BigDecimal::add);
			return amount.compareTo(availableAmount) > 0
					? "Impossible to provide a quote. Please try later"
					: null;

		};

		return Arrays.asList(edgesVerifier, incrementVerifier, amountAvailableVerifier);
	}

	private static void printError(String errorDescription) {
		System.out.println(errorDescription);
	}

	private static void printLoanInfo(LoanInfo result) {
		NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.UK);
		NumberFormat percentFormat = NumberFormat.getPercentInstance(Locale.UK);
		percentFormat.setMinimumFractionDigits(1);

		System.out.println("Requested amount: " + currencyFormat.format(result.getRequestedAmount()));
		System.out.println("Rate: " + percentFormat.format(result.getRate()));
		System.out.println("Monthly repayment: " + currencyFormat.format(result.getMonthlyRepayment()));
		System.out.println("Total repayment: " + currencyFormat.format(result.getTotalRepayment()));
	}
}
