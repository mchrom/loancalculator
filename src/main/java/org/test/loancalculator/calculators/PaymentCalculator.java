package org.test.loancalculator.calculators;

import java.math.BigDecimal;

/**
 * Calculates monthly payments for given requested amount, interest rate and loan period
 */
public interface PaymentCalculator {
	BigDecimal calculateMonthlyRepayment(BigDecimal totalAmount, BigDecimal rate, int period);
}
