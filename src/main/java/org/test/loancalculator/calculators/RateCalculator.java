package org.test.loancalculator.calculators;

import java.math.BigDecimal;

/**
 * Interface for rate calculators.
 * Rate calculator gets requested loan amount as a parameter and calculate interest rate
 * for this amount.
 */
public interface RateCalculator {
	BigDecimal calculateRate(BigDecimal amount);
}
