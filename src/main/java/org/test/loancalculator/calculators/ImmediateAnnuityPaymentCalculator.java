package org.test.loancalculator.calculators;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * {@link PaymentCalculator} implementation for Annuity-immediate payments.
 *
 * Calculates monthly payment following way:
 * P = AF * LA
 * where:
 * AF - annuity factor
 * LA - loan amount
 */
public class ImmediateAnnuityPaymentCalculator implements PaymentCalculator {

	private static final MathContext COEFFICIENT_CALCULATION_CONTEXT = MathContext.DECIMAL64;

	@Override
	public BigDecimal calculateMonthlyRepayment(BigDecimal totalAmount, BigDecimal rate, int period) {
		BigDecimal monthRate = calcMonthRate(rate);
		BigDecimal annuityFactor = calcAnnuityFactor(monthRate, period);
		return totalAmount.multiply(annuityFactor);
	}

	private BigDecimal calcMonthRate(BigDecimal yearRate) {
		return BigDecimal.valueOf(Math.pow(yearRate.doubleValue() + 1, 1.0 / 12) - 1);
	}

	/**
	 *  Annuity factor is calculated with formula
	 *
	 * <pre>
	 *              r
	 *   K = ----------------
	 *        1 - (1 + r)^-p
	 * </pre>
	 *
	 * where
	 *    r - monthRate
	 *    p - period (in months)
	 * <br/>
	 * Formula details <a href="http://financeformulas.net/Annuity-Payment-Factor.html">here</a>
	 */
	private BigDecimal calcAnnuityFactor(BigDecimal monthRate, int period) {


		BigDecimal t = BigDecimal.ONE
				.add(monthRate)
				.pow(period, COEFFICIENT_CALCULATION_CONTEXT);

		t = BigDecimal.ONE.divide(t, COEFFICIENT_CALCULATION_CONTEXT);

		return monthRate.divide(BigDecimal.ONE.subtract(t), COEFFICIENT_CALCULATION_CONTEXT);
	}
}
