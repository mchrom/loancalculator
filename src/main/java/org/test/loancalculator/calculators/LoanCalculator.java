package org.test.loancalculator.calculators;

import org.test.loancalculator.objects.CalculationResult;
import org.test.loancalculator.objects.LoanInfo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;

public class LoanCalculator {
	private final RateCalculator rateCalculator;
	private final PaymentCalculator paymentCalculator;
	private final List<LoanParamsVerifier> verifiers;

	public LoanCalculator(RateCalculator rateCalculator, PaymentCalculator paymentCalculator, List<LoanParamsVerifier> verifiers) {
		this.rateCalculator = rateCalculator;
		this.paymentCalculator = paymentCalculator;
		this.verifiers = verifiers;
	}

	public CalculationResult calculateLoan(BigDecimal amount, int period) {
		String error = verifiers.stream()
				.map(verifier -> verifier.verifyParams(amount, period))
				.filter(Objects::nonNull)
				.findAny()
				.orElse(null);

		if (error != null) {
			return CalculationResult.error(error);
		}

		BigDecimal rate = rateCalculator.calculateRate(amount);
		BigDecimal repayment = paymentCalculator.calculateMonthlyRepayment(amount, rate, period);
		BigDecimal totalRepayment = repayment.multiply(BigDecimal.valueOf(period));

		LoanInfo loanInfo = new LoanInfo(
				amount,
				rate.setScale(3, RoundingMode.HALF_UP),
				repayment.setScale(2, RoundingMode.HALF_UP),
				totalRepayment.setScale(2, RoundingMode.HALF_UP));
		return CalculationResult.success(loanInfo);
	}
}
