package org.test.loancalculator.calculators;

import org.test.loancalculator.objects.Lender;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation of {@link RateCalculator} interface which calculates interest rate based on info from lenders pool.
 *
 * Calculator uses list of lenders sorted by rate and takes lenders from the head of this list until their total
 * available amount is enough to fill requested loan amount.
 * Calculator saves amount borrowed from each lender and then calculates result interest rate as volume-weighted sum
 * of lenders rates.
 */
public class VolumeWeightedRateCalculator implements RateCalculator {

	private static final MathContext RATE_CALCULATION_CONTEXT = MathContext.DECIMAL64;

	/**
	 * Lenders field contains lenders pool sorted in descending order of interest
	 */
	private final List<Lender> lenders;

	public VolumeWeightedRateCalculator(Collection<Lender> unorderedLenders) {
		lenders = unorderedLenders.stream()
				.filter(lender -> lender.getAvailableAmount().compareTo(BigDecimal.ZERO) > 0)
				.sorted(Comparator.comparing(Lender::getRate))
				.collect(Collectors.toList());
	}

	@Override
	public BigDecimal calculateRate(BigDecimal requestedAmount) {
		Iterator<Lender> lenderIterator = lenders.iterator();
		Map<Lender, BigDecimal> borrowedAmounts = new IdentityHashMap<>();
		BigDecimal sum = BigDecimal.ZERO;

		while (sum.compareTo(requestedAmount) < 0 && lenderIterator.hasNext()) {
			Lender lender = lenderIterator.next();
			BigDecimal needed = requestedAmount.subtract(sum);
			BigDecimal borrowed = needed.min(lender.getAvailableAmount());
			borrowedAmounts.put(lender, borrowed);
			sum = sum.add(borrowed);
		}

		if (sum.compareTo(requestedAmount) < 0) {
			throw new IllegalArgumentException("Lenders pool total amount is less than requested loan");
		}

		return calculateVolumeWeightedRate(borrowedAmounts, sum);
	}

	private BigDecimal calculateVolumeWeightedRate(Map<Lender, BigDecimal> borrowedAmounts, BigDecimal totalAmount) {
		BigDecimal res = BigDecimal.ZERO;
		for (Map.Entry<Lender, BigDecimal> entry : borrowedAmounts.entrySet()) {
			Lender lender = entry.getKey();
			BigDecimal amount = entry.getValue();
			BigDecimal weightedValue = lender.getRate()
					.multiply(amount)
					.divide(totalAmount, RATE_CALCULATION_CONTEXT);
			res = res.add(weightedValue);
		}

		return res;
	}
}
