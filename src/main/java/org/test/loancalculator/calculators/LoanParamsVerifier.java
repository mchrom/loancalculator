package org.test.loancalculator.calculators;

import java.math.BigDecimal;

/**
 * Verifier takes loan parameters and checks if this params appropriate.
 * Returns error description for wrong params or null if parameters are OK.
 */
public interface LoanParamsVerifier {
	String verifyParams(BigDecimal amount, int period);
}
