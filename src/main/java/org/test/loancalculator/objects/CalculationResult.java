package org.test.loancalculator.objects;

public class CalculationResult {
	private final LoanInfo result;
	private final String errorDescription;

	private CalculationResult(LoanInfo result, String errorDescription) {
		this.result = result;
		this.errorDescription = errorDescription;
	}

	public boolean isSuccess() {
		return result != null;
	}

	public LoanInfo getResult() {
		return result;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public static CalculationResult success(LoanInfo result) {
		return new CalculationResult(result, null);
	}

	public static CalculationResult error(String errorDescription) {
		return new CalculationResult(null, errorDescription);
	}
}
