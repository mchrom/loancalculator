package org.test.loancalculator.objects;

import java.math.BigDecimal;

public class Lender {
	private final String name;
	private final BigDecimal rate;
	private final BigDecimal availableAmount;

	public Lender(String name, BigDecimal rate, BigDecimal availableAmount) {
		this.name = name;
		this.rate = rate;
		this.availableAmount = availableAmount;
	}

	public String getName() {
		return name;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public BigDecimal getAvailableAmount() {
		return availableAmount;
	}
}
