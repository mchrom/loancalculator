package org.test.loancalculator;

import org.test.loancalculator.objects.Lender;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;

public class LenderPoolLoader {

	private static final String FIELD_DELIMETER = ",";

	public static Collection<Lender> loadFromFile(File file) throws IOException {
		Collection<Lender> res = new ArrayList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			String line = reader.readLine();
			if (line != null) {
				//skip header if file is not empty
				line = reader.readLine();
			}

			while (line != null) {
				String[] fields = line.split(FIELD_DELIMETER);
				Lender lender = new Lender(
						fields[0],
						new BigDecimal(fields[1]),
						new BigDecimal(fields[2]).setScale(2, RoundingMode.HALF_UP)
				);

				//skip wrong lines
				if (lender.getAvailableAmount().compareTo(BigDecimal.ZERO) > 0
						&& lender.getRate().compareTo(BigDecimal.ZERO) > 0) {
					res.add(lender);
				}
				line = reader.readLine();
			}

			return res;
		}
	}
}
