package org.test.loancalculator;

import org.junit.Before;
import org.junit.Test;
import org.test.loancalculator.calculators.ImmediateAnnuityPaymentCalculator;
import org.test.loancalculator.calculators.LoanCalculator;
import org.test.loancalculator.calculators.LoanParamsVerifier;
import org.test.loancalculator.calculators.VolumeWeightedRateCalculator;
import org.test.loancalculator.objects.CalculationResult;
import org.test.loancalculator.objects.Lender;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class LoanCalculatorTest {

	private LoanCalculator calculator;

	@Before
	public void init() {
		//total sum - 2400
		Collection<Lender> lenders = Arrays.asList(
				new Lender("Bob", BigDecimal.valueOf(0.075), BigDecimal.valueOf(640)),
				new Lender("Jane", BigDecimal.valueOf(0.069), BigDecimal.valueOf(480)),
				new Lender("Fred", BigDecimal.valueOf(0.071), BigDecimal.valueOf(520)),
				new Lender("Mary", BigDecimal.valueOf(0.104), BigDecimal.valueOf(170)),
				new Lender("John", BigDecimal.valueOf(0.081), BigDecimal.valueOf(320)),
				new Lender("Dave", BigDecimal.valueOf(0.074), BigDecimal.valueOf(140)),
				new Lender("Angela", BigDecimal.valueOf(0.071), BigDecimal.valueOf(60)),
				new Lender("Dan", BigDecimal.valueOf(0.075), BigDecimal.valueOf(70))
		);

		calculator = new LoanCalculator(
				new VolumeWeightedRateCalculator(lenders),
				new ImmediateAnnuityPaymentCalculator(),
				createVerifiers(lenders)
		);
	}

	@Test
	public void exampleTest() {
		CalculationResult res = calculator.calculateLoan(new BigDecimal("1000.00"), 36);
		assertTrue(res.isSuccess());
		assertEquals(res.getResult().getRequestedAmount(), new BigDecimal("1000.00"));
		assertEquals(res.getResult().getRate(), new BigDecimal("0.070"));
		assertEquals(res.getResult().getMonthlyRepayment(), new BigDecimal("30.78"));
		assertEquals(res.getResult().getTotalRepayment(), new BigDecimal("1108.10"));
	}

	@Test
	public void loan2400Test() {
		CalculationResult res = calculator.calculateLoan(new BigDecimal("2400.00"), 36);
		assertTrue(res.isSuccess());
		assertEquals(res.getResult().getRequestedAmount(), new BigDecimal("2400.00"));
		assertEquals(res.getResult().getRate(), new BigDecimal("0.076"));
		assertEquals(res.getResult().getMonthlyRepayment(), new BigDecimal("74.45"));
		assertEquals(res.getResult().getTotalRepayment(), new BigDecimal("2680.15"));
	}

	@Test
	public void notEnoughLendersTest() {
		CalculationResult res = calculator.calculateLoan(BigDecimal.valueOf(2500), 36);
		assertFalse(res.isSuccess());
		assertEquals(res.getErrorDescription(), "notEnoughMoneyErr");
	}

	@Test
	public void emptyLendersTest() {
		LoanCalculator calc = new LoanCalculator(
				new VolumeWeightedRateCalculator(Collections.emptyList()),
				new ImmediateAnnuityPaymentCalculator(),
				createVerifiers(Collections.emptyList())
		);
		CalculationResult res = calc.calculateLoan(BigDecimal.valueOf(1000), 36);
		assertFalse(res.isSuccess());
		assertEquals(res.getErrorDescription(), "notEnoughMoneyErr");
	}

	@Test
	public void oneLenderTest() {
		Collection<Lender> lenders = Arrays.asList(
				new Lender("Bob", BigDecimal.valueOf(0.078), BigDecimal.valueOf(10000)),
				new Lender("Bob", BigDecimal.valueOf(0.085), BigDecimal.valueOf(20000))
		);

		LoanCalculator calc = new LoanCalculator(
				new VolumeWeightedRateCalculator(lenders),
				new ImmediateAnnuityPaymentCalculator(),
				createVerifiers(lenders)
		);
		CalculationResult res = calc.calculateLoan(new BigDecimal("8000.00"), 36);
		assertTrue(res.isSuccess());
		assertEquals(res.getResult().getRequestedAmount(), new BigDecimal("8000.00"));
		assertEquals(res.getResult().getRate(), new BigDecimal("0.078"));
		assertEquals(res.getResult().getMonthlyRepayment(), new BigDecimal("248.98"));
		assertEquals(res.getResult().getTotalRepayment(), new BigDecimal("8963.13"));
	}

	@Test
	public void badAmountsTest() {
		CalculationResult res = calculator.calculateLoan(BigDecimal.valueOf(1550), 36);
		assertFalse(res.isSuccess());
		assertEquals(res.getErrorDescription(), "stepErr");

		res = calculator.calculateLoan(BigDecimal.valueOf(15500), 36);
		assertFalse(res.isSuccess());
		assertEquals(res.getErrorDescription(), "edgesErr");

		res = calculator.calculateLoan(BigDecimal.valueOf(500), 36);
		assertFalse(res.isSuccess());
		assertEquals(res.getErrorDescription(), "edgesErr");
	}

	private static List<LoanParamsVerifier> createVerifiers(Collection<Lender> lenders) {
		return Arrays.asList(
				(amount, period) -> amount.compareTo(BigDecimal.valueOf(1000)) < 0 || amount.compareTo(BigDecimal.valueOf(15000)) > 0 ? "edgesErr" : null,
				(amount, period) -> amount.remainder(BigDecimal.valueOf(100)).compareTo(BigDecimal.ZERO) != 0 ? "stepErr" : null,
				(amount, period) -> {
					BigDecimal availableAmount = lenders.stream()
							.map(Lender::getAvailableAmount)
							.reduce(BigDecimal.ZERO, BigDecimal::add);
					return amount.compareTo(availableAmount) > 0 ? "notEnoughMoneyErr" : null;
				}
		);
	}

}
