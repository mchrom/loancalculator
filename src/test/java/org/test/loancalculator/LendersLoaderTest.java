package org.test.loancalculator;

import org.junit.Test;
import org.test.loancalculator.objects.Lender;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LendersLoaderTest {

	@Test
	public void testEmpty() throws IOException {
		File file = getTestFile("empty.csv");
		Collection<Lender> lenders = LenderPoolLoader.loadFromFile(file);
		assertTrue(lenders.isEmpty());
	}

	@Test
	public void test1() throws IOException {
		List<String> expected = Arrays.asList(
				"Bob,0.075,640.00",
				"Jane,0.069,480.00",
				"Fred,0.071,520.00"
		);

		File file = getTestFile("tst1.csv");
		Collection<Lender> lenders = LenderPoolLoader.loadFromFile(file);
		compareCollections(expected, lenders);
	}

	@Test
	public void test2() throws IOException {
		List<String> expected = Arrays.asList(
				"Mary,0.104,170.00",
				"Angela,0.071,60.00"
		);

		File file = getTestFile("tst2.csv");
		Collection<Lender> lenders = LenderPoolLoader.loadFromFile(file);
		compareCollections(expected, lenders);
	}

	@Test
	public void test3() throws IOException {
		File file = getTestFile("tst3.csv");
		Collection<Lender> lenders = LenderPoolLoader.loadFromFile(file);
		assertTrue(lenders.isEmpty());
	}

	private void compareCollections(List<String> expected, Collection<Lender> lenders) {
		assertEquals(lenders.size(), expected.size());
		List<String> actual = lenders.stream()
				.map(lender -> String.join(",",lender.getName(), lender.getRate().toString(), lender.getAvailableAmount().toString()))
				.collect(Collectors.toList());
		assertEquals(expected, actual);
	}

	private static File getTestFile(String name) {
		String fileName = LendersLoaderTest.class.getResource(name).getFile();
		return new File(fileName);
	}
}
